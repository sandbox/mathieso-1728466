<?php
/**
 * @file
 * Tests for the domain reference module.
 */

class DomainReferenceTestCase extends DrupalWebTestCase {
  
  protected $admin_user;
  //In Australia, we had Dick and Dora, not Dick and Jane.
  protected $dick_user;
  protected $dora_user;
  protected $domains;

  public static function getInfo() {
    return array(
      'name' => 'Domain reference functionality',
      'description' => 'Create, view, edit, delete, and change domain references,
        and verify their consistency in the database.',
      'group' => 'Domain reference',
    );
  }  
  
  /**
   * Enable modules and create a user with specific permissions.
   */
  function setUp($list = array()) {
    //Adapted from domain.test.
    // Prevent simpletest from vomiting on bootstrap if there
    // are required submodules.
    $modules = _domain_bootstrap_modules();
    module_enable($modules);
    $modules = array_merge($modules, $list);
    parent::setUp($modules);

    // Account for drush and other automated testing to set a proper http value.
    if (empty($_SERVER['HTTP_HOST']) || $_SERVER['HTTP_HOST'] == 'default') {
      $_SERVER['HTTP_HOST'] = 'example.com';
    }
    // Build the {domain} table for this test run.
    db_query("TRUNCATE {domain}");
    db_query("TRUNCATE {domain_export}");
    domain_set_primary_domain();
    db_query("UPDATE {domain} SET sitename = 'TestDomainSitename' WHERE is_default = 1");

    // Set the primary domain to active. This step should let us run
    // tests from any valid domain. See http://drupal.org/node/1144632.
    $GLOBALS['_domain'] = domain_default(TRUE);
    drupal_static_reset('domain_initial_domain');
    domain_initial_domain($GLOBALS['_domain']);

    module_load_include('inc', 'node', 'content_types');
    module_load_include('inc', 'node', 'node.pages');
    module_load_include('inc', 'field', 'field.crud');
    
    module_enable(array('options', 'domain_reference'));
    
    // Create users.
    $this->admin_user = $this->drupalCreateUser(
      array(
        'administer content types', 
        'administer nodes', 
        'bypass node access',
        'administer permissions',
        'administer users',
        'access user profiles',
        'administer domains',
        )
    );
    $this->dick_user = $this->drupalCreateUser();
    $this->dora_user = $this->drupalCreateUser();
    //Log in admin user.
    $this->drupalLogin($this->admin_user);
    //Create some domains.
    $this->domains = $this->domainCreateDomains(
        array('swagman_domain', 'squatter_domain', 
          'trooper1_domain', 'trooper2_domain', 'trooper3_domain', 
        )
    );
  }

  /**
   * Helper function to create domains.
   *
   * @param $subdomains
   *   An array of subdomains to create.
   * @param $base
   *   A base domain string, in the format 'example.com'.
   *
   * @return
   *  An array of created domains.
   */
  public function domainCreateDomains($subdomains = array('one', 'two', 'three'), $base = NULL) {
    $setup = drupal_map_assoc($subdomains);
    $base_domain = $base;
    if (empty($base_domain) && !empty($_SERVER['HTTP_HOST'])) {
      $base_domain = $_SERVER['HTTP_HOST'];
    }
    if (empty($base_domain)) {
      $base_domain = 'example.com';
    }
    $i = 0;
    $domains = array();
    foreach ($setup as $subdomain) {
      $record = array(
        'sitename' => $subdomain,
        'subdomain' => $subdomain . '.' . $base_domain,
        'valid' => 1,
        'is_default' => 0,
        'weight' => $i++,
        'scheme' => 'http',
      );
      $domains[$subdomain] = domain_save($record, $record);
    }
    return $domains;
  }

  /**
   * Creates a domain reference field from an array of settings values.
   *
   * All values have defaults, only need to specify values that need to be
   * different.
   */
  protected function createDomainReferenceField($values = array()) {
    extract($values);

    $field_name = !empty($field_name) ? $field_name : 'field_test';
    $entity_type = !empty($entity_type) ? $entity_type : 'user';
    $bundle = !empty($bundle) ? $bundle : 'user';
    $label = !empty($label) ? $label : 'Label of DOOM';
    $field_type = !empty($field_type) ? $field_type : 'domain_reference';
    $repeat = !empty($repeat) ? $repeat : 0;
    $widget_type = !empty($widget_type) ? $widget_type : 'options_buttons';

    $field = array(
      'field_name' => $field_name,
      'type' => $field_type,
      'cardinality' => !empty($repeat) ? FIELD_CARDINALITY_UNLIMITED : 1,
    );
    $instance = array(
      'entity_type' => $entity_type,
      'field_name' => $field_name,
      'label' => $label,
      'bundle' => $bundle,
      // Move the date right below the title.
      'weight' => -4,
      'widget' => array(
        'type' => $widget_type,
      ),
    );

    $instance['display'] = array(
      'default' => array(
        'label' => 'above',
        'type' => 'domain_reference_show_name',
        'module' => 'domain_reference',
        'weight' => 0 ,
      ),
      'teaser' => array(
        'label' => 'above',
        'type' => 'date_default',
        'weight' => 1,
        'module' => 'domain_reference',
      ),
    );
    
    field_info_cache_clear(TRUE);
    field_cache_clear(TRUE);    

    $field = field_create_field($field);
    $instance = field_create_instance($instance);

    field_info_cache_clear(TRUE);
    field_cache_clear(TRUE);

    // Look at how the field got configured.
    if ( $bundle == 'user' ) {
      $this->drupalGet("admin/config/people/accounts/fields");
      $this->drupalGet("admin/config/people/accounts/fields/$field_name");
      $this->drupalGet('admin/config/people/accounts/display');
    }
    else {
      //Assume it's a content type.
      $this->drupalGet("admin/structure/types/manage/$bundle/fields/$field_name");
      $this->drupalGet("admin/structure/types/manage/$bundle/display");
    }
  }

  /**
   * @todo.
   */
  protected function deleteDomainReferenceField($label, $bundle = 'user', $bundle_name = 'User') {
    if ( $bundle == 'user' ) {
      $this->drupalGet('admin/config/people/accounts/fields');
    }
    else {
      //Assume it's a content type.
      $this->drupalGet("admin/structure/types/manage/$bundle/fields");
    }
    $this->clickLink('delete');
    $this->drupalPost(NULL, NULL, t('Delete'));
    $this->assertText(t("The field $label has been deleted"), t('Removed field'));
  }

  /**
   * Test whether domain reference fields can be added to the user entity.
   */
  public function testFieldCreationUserEntity() {
    //Create field with default settings.
    //Field is on the user entity. Radio buttons.
    $this->createDomainReferenceField();
    //Field on user field list?
    $this->drupalGet('admin/config/people/accounts/fields');
    $this->assertText(
        'Domain reference', 
        'Domain reference field created on user entity.',
        'Field creation'
    ) ;
    $this->assertText(
        'field_test', 
        'Correct machine name of domain reference field on user entity.',
        'Field creation'
    );
    $this->assertText(
        'Check boxes/radio buttons', 
        'Correct widget type on user entity.',
        'Field creation'
    );
  }    

  /**
   * Test whether domain reference fields can be added to a content type.
   */
  public function testFieldCreationContentType() {
    //Create a content type.
    $edit = array();
    $edit['name'] = 'Dog';
    $edit['type'] = 'dog';
    $this->drupalPost('admin/structure/types/add', $edit, t('Save content type'));
    $this->assertText('The content type Dog has been added.', 'Content type added.');
    $this->createDomainReferenceField(
        array(
          'field_name' => 'field_dog_domain',
          'label' => 'Dog domain',
          'bundle' => 'dog',
          'widget' => array(
            'type' => 'options_select',
          ),
        )
    );
    //Field on user field list?
    $this->drupalGet("admin/structure/types/manage/dog/fields");
    $this->assertText(
        'Domain reference', 
        'Domain reference field created on content type.',
        'Field creation'
    ) ;
    $this->assertText(
        'field_dog_domain', 
        'Correct machine name of domain reference field on content type.',
        'Field creation'
    );
    $this->assertText(
        'Select list',
        'Correct widget type on content type.',
        'Field creation'
    );
  }    
//  //Add a reference to a domain.
//    $edit = array();
//    $edit["body[und][0][value]"] = $this->randomName(16);    
//    //Go to user 
//    $dick_uid = $this->dick_user->uid;
////    $this->drupalPost("user/$dick_uid/edit", );
////    $this->assertText($text, $message)
//  }
  
  
}