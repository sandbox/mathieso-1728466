<?php
/**
 * @files
 * Given a field type, grabs metadata about instances of that type.
 */
class InstanceMetadataGrabber {
  /**
   * Singleton pattern. 
   * See http://www.ibm.com/developerworks/library/os-php-designptrns/
   */
  public static $instance = NULL;

  /**
   * Make an InstanceMetadataGrabber object.
   * @return InstanceMetadataGrabber object.
   */
  public static function getOneGrabber($field_type_to_find) {
    if( ! isset(self::$instance) ) {
      self::$instance = new InstanceMetadataGrabber($field_type_to_find);
    }
    return self::$instance;
  }
  
  /**
   * Constructor not callable outside class.
   * @param string $field_type_to_find Grab data about instances of this type.
   */
  protected function __construct($field_type_to_find) {
    $this->field_type = $field_type_to_find;
    $this->field_instance_metadata = $this->loadInstanceMetadata();
  }
  
  /**
   * Cache for the loaded metadata.
   * @var array
   */
  protected $field_instance_metadata = NULL;
  
  /**
   * Field type of instances.
   * @var string
   */
  protected $field_type = NULL;
  
  /**
   * Get metadata about instances, entities, fields, and bundles, for a field type.
   * @param string $field_type Field type name.
   * @return array Metadata, indexed by instance id, sorted by bundle label and 
   *  then instance label. 
   */
  protected function loadInstanceMetadata() {
    $er_instances = array();
    $all_instances = field_info_instances();
    $all_fields = field_info_fields();
    $all_entities = entity_get_info();
    $all_bundles = field_info_bundles();
    foreach ( $all_instances as $entity_name => $bundles ) {
      foreach ( $bundles as $bundle_name => $fields ) {
        foreach ( $fields as $field_name => $field ) {
          $field_type = $all_fields[$field_name]['type'];
          if ( $field_type == $this->field_type ) {
            $er_instance = array();
            $er_instance['entity_name'] = $entity_name;
            $er_instance['entity_label'] = $all_entities[$entity_name]['label'];
            $er_instance['entity_description'] = 
                isset( $all_entities[$entity_name]['description'] )
                  ? $all_entities[$entity_name]['description']
                  : '';
            $er_instance['bundle_name'] = $bundle_name;
            $er_instance['bundle_label'] = $all_bundles[$entity_name][$bundle_name]['label'];
            $er_instance['field_name'] = $field_name;
            $er_instance['instance_id'] = $field['id'];
            $er_instance['instance_label'] = $field['label'];
            $er_instance['instance_description'] = $field['description'];
            $er_instances[$er_instance['instance_id']] = $er_instance;
          } //End this is a domain reference instance.
        } //End for each field in bundle (i.e., an instance).
      } //End foreach bundle
    } //End foreach instance
    if ( sizeof($er_instances) > 1 ) {
      uasort($er_instances, 'InstanceMetadataGrabber::compareInstances');
    }
    return $er_instances;
  } //End loadInstanceMetadata.
  
  /**
   * Comparison callback for sorting instance metadata.
   * @param array $a
   * @param array $b
   * @return int -1 if $a < $b, 0 if same, 1 if $a > $b.
   */
  protected static function compareInstances($a, $b) {
    if ( $a['bundle_label'] < $b['bundle_label'] ) {
      return -1;
    }
    if ( $a['bundle_label'] > $b['bundle_label'] ) {
      return 1;
    }
    //Bundles are the same.
    if ( $a['instance_label'] < $b['instance_label'] ) {
      return -1;
    }
    if ( $a['instance_label'] > $b['instance_label'] ) {
      return 1;
    }
    return 0;
  } //End compareInstances.
  
  /**
   * Return array for metadata for all instances.
   * @return array Metadata.
   */
  public function getAllMetadata() {
    return $this->field_instance_metadata;
  }

  /**
   * Return metadata for one instance.
   * @param int $instance_id Instance id.
   * @return array Metadata.
   */
  public function getInstanceMetadata($instance_id) {
    return $this->doesInstanceIdExist($instance_id) 
        ? $this->field_instance_metadata[$instance_id]
        : NULL;
  }
  
  /**
   * Does an instance with a given id exist?
   * @param int $instance_id Instance id.
   * @return boolean True if exists, else false.
   */
  public function doesInstanceIdExist($instance_id) {
    return isset( $this->field_instance_metadata[$instance_id] );
  }
  
  /**
   * Get the name of the field an instance is an instance of.
   * This is the machine name of the field, not the label.
   * @param int $instance_id Instance id.
   * @return string Field name or NULL if not found.
   */
  public function getFieldName($instance_id) {
    return $this->doesInstanceIdExist($instance_id) 
        ? $this->field_instance_metadata[$instance_id]['field_name']
        : NULL;
  }

  /**
   * Get the name of the entity an instance is attached to.
   * @return string Name.
   */
  public function getEntityName($instance_id) {
    return $this->field_instance_metadata[$instance_id]['entity_name'];
  }
  
  /**
   * Get the label of the entity an instance is attached to.
   * @return string Label.
   */
  public function getEntityLabel($instance_id) {
    return $this->field_instance_metadata[$instance_id]['entity_label'];
  }
  
  /**
   * Get the description of the entity an instance is attached to.
   * @return string Description.
   */
  public function getEntityDescription($instance_id) {
    return $this->field_instance_metadata[$instance_id]['entity_description'];
  }
  
  /**
   * Get the name of an instance's bundle.
   * @return string Name.
   */
  public function getBundleName($instance_id) {
    return $this->field_instance_metadata[$instance_id]['bundle_name'];
  }
  
  /**
   * Get the label of an instance's bundle.
   * @return string Label.
   */
  public function getBundleLabel($instance_id) {
    return $this->field_instance_metadata[$instance_id]['bundle_label'];
  }
  
  /**
   * Get an instance's label.
   * @return string Label.
   */
  public function getInstanceLabel($instance_id) {
    return $this->field_instance_metadata[$instance_id]['instance_label'];
  }
  
  /**
   * Get an instance's description.
   * @return string Description.
   */
  public function getInstanceDescription($instance_id) {
    return $this->field_instance_metadata[$instance_id]['instance_description'];
  }
  
  /**
   * Return number of instances.
   * @return int Number of fields.
   */
  public function countInstances() {
    return sizeof($this->field_instance_metadata);
  }

  
  public function getInstanceIds() {
    $ids = array();
    foreach ( $this->field_instance_metadata as $instance ) {
      $ids[] = $instance['instance_id'];
    }
    return $ids;
  }
  
}